// #![feature(test)]
// extern crate test;

use regex::Regex;
use reqwest::{header::USER_AGENT, Client, StatusCode};
use url::Url;

use chardet;
use encoding::{label::encoding_from_whatwg_label, DecoderTrap};
use std::{cmp, io::Read, time::Duration};

use failure::Fail;
use lazy_static::lazy_static;

pub struct Browser {
    client: Client,
    user_agent: String,
}

pub struct RequestResponse {
    pub data: String,
    pub final_url: Url,
    pub http_status: StatusCode,
}

lazy_static! {
    static ref DETECT_CHARSET_REGEX: Regex =
        Regex::new("(?i)(charset|encoding)=\"?(.*?)\"").unwrap();
}

#[derive(Fail, Debug)]
pub enum BrowserError {
    #[fail(display = "Failed to crawl page with status code: {}", _0)]
    FailedToGet(StatusCode),

    #[fail(display = "Failed to read response into buffer: {}", _0)]
    FailedToReadResponse(#[fail(cause)] std::io::Error),

    #[fail(display = "Failed to send reqwest: {}", _0)]
    ReqwestSendError(#[fail(cause)] reqwest::Error),
}

impl Browser {
    pub fn new(user_agent: String, timeout: Duration) -> Self {
        let client = Client::builder().timeout(timeout).build().unwrap();
        Browser { client, user_agent }
    }

    fn make_request(&self, url: &Url) -> Result<reqwest::Response, BrowserError> {
        self.client
            .get(url.clone())
            .header(USER_AGENT, &self.user_agent)
            .send()
            .map_err(BrowserError::ReqwestSendError)
    }

    fn parse_response(
        &self,
        response: &mut reqwest::Response,
    ) -> Result<RequestResponse, BrowserError> {
        if !response.status().is_success() {
            Err(BrowserError::FailedToGet(response.status()))?;
        }
        let mut result_data = String::new();
        let mut response_buffer: Vec<u8> = Vec::with_capacity(5000);

        response
            .read_to_end(&mut response_buffer)
            .map_err(BrowserError::FailedToReadResponse)?;

        // 500 is average length in html head where meta tag with encoding is placed
        let range = cmp::min(1500, response_buffer.len());
        let temp_html_head = String::from_utf8_lossy(&response_buffer[0..range]);

        let encoding = DETECT_CHARSET_REGEX
            .captures(&temp_html_head)
            .and_then(|c| c.get(2))
            .map_or_else(
                || None,
                |c| {
                    let r = c.as_str().to_lowercase();
                    match r.chars().count() > 15 {
                        true => None,
                        false => Some(r),
                    }
                },
            )
            .unwrap_or_else(|| chardet::detect(&response_buffer).0)
            .to_lowercase();

        if let Some(coder) = encoding_from_whatwg_label(chardet::charset2encoding(&encoding)) {
            result_data = coder
                .decode(&mut response_buffer, DecoderTrap::Ignore)
                .ok()
                .unwrap_or_default();
        }

        Ok(RequestResponse {
            data: result_data,
            final_url: response.url().clone(),
            http_status: response.status(),
        })
    }

    pub fn get(&self, url: Url) -> Result<RequestResponse, BrowserError> {
        let mut response = self.make_request(&url)?;
        self.parse_response(&mut response)
    }
}

// #[cfg(test)]
// mod tests {
//     use crate::Browser;
//     use reqwest::StatusCode;
//     use std::time::Duration;
//     use test::Bencher;
//     use url::Url;

//     #[test]
//     fn test_sports() {
//         let browser = Browser::new(
//             "Googlebot/2.1 (+http://www.google.com/bot.html)",
//             Duration::from_secs(10),
//         );
//         let url = Url::parse("https://www.sports.ru/football/1083245592.html").unwrap();
//         let result = browser.get(url);

//         assert_eq!(result.is_ok(), true);
//         assert_eq!(result.as_ref().unwrap().http_status, StatusCode::OK);

//         println!("CONTENT:\n{}", result.unwrap().data);
//     }

//     #[bench]
//     fn test_speed(b: &mut Bencher) {
//         let browser = Browser::new(
//             "Googlebot/2.1 (+http://www.google.com/bot.html)",
//             Duration::from_secs(10),
//         );
//         let url = Url::parse("https://doc.rust-lang.org/std/time/struct.Duration.html").unwrap();
//         let mut response = browser.make_request(&url).unwrap();
//         b.iter(|| {
//             browser.parse_response(&mut response).unwrap();
//         });
//     }

//     #[test]
//     fn test_google() {
//         let browser = Browser::new(
//             "Googlebot/2.1 (+http://www.google.com/bot.html)",
//             Duration::from_secs(10),
//         );
//         let url = Url::parse("https://doc.rust-lang.org/std/time/struct.Duration.html").unwrap();
//         let result = browser.get(url);

//         assert_eq!(result.is_ok(), true);
//         assert_eq!(result.as_ref().unwrap().http_status, StatusCode::OK);

//         println!("CONTENT:\n{}", result.unwrap().data);
//     }
// }
